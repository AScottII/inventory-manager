import tkinter
from tkinter.ttk import Notebook
from tkinter.ttk import Frame

import Queries as Q

class SpecificationWindow(tkinter.Tk):

    def __init__(self, mainWindow, model):
        super().__init__()
        self.mainWin = mainWindow
        self.model = model
        self.title('Specifications')
        self.geometry('400x400')
        self.resizable(False,False)

        self.tab_parent = Notebook(self)
        self.specTab = Frame(self.tab_parent)
        self.tab_parent.add(self.specTab)

        self.type,self.computerData = Q.getSpecs(model)

        self.text = self.formatString()

        self.label = tkinter.Label(master=self.specTab,text=self.text,font=("Arial",12))

        self.okbutton = tkinter.Button(self.specTab,text="OK",command=lambda: self.exitButton())

        self.tab_parent.pack(expand=True)
        self.specTab.pack(expand=True)
        self.label.pack(expand=True)
        self.okbutton.pack()

        self.mainloop()

    def exitButton(self):
        self.destroy()

    def formatString(self):
        output = ""
        if not self.type in ["Desktop","Monitor","Laptop"]:
            return
        match self.type:
            case "Desktop":
                output = "Hard Drive Capacity: %s\nRAM: %s\n" % (self.computerData[0],self.computerData[1])
                output += "Video Ports: %s\nNumber of USB Ports: %s" % (self.computerData[2], self.computerData[3])
            case "Monitor":
                output = "Resolution: %s\nScreen Size: %s\n" % (self.computerData[0], self.computerData[1])
                output += "Video Ports: %s\nYear Made: %s" % (self.computerData[2],self.computerData[3])
            case "Laptop":
                output = "Resolution: %s\nScreen Size: %s\n" % (self.computerData[0], self.computerData[1])
                output += "Hard Drive Capacity: %s\nRAM: %s\n" % (self.computerData[2],self.computerData[3])
                output += "Video Ports: %s\nNumber of USB Ports: %s\n" % (self.computerData[4], self.computerData[5])
                output += "Year Made: %s" % self.computerData[6]

        return output

    

        
