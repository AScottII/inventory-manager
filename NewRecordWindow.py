import tkinter
from tkinter.ttk import Notebook
from tkinter.ttk import Frame

import Queries as Q
from NotificationWindow import NotificationWindow

class NewRecordWindow(tkinter.Tk):

    def __init__(self,mainWindow):
        super().__init__()
        self.mainWin = mainWindow
        self.title('Add New Computer')
        self.geometry('500x300')
        self['bg'] = '#BFBFBF'

        # self <- tab_parent <- addComputerTab
        self.tab_parent = Notebook(self)
        self.addComputerTab = Frame(self.tab_parent)
        self.tab_parent.add(self.addComputerTab)

        self.clabel1 = tkinter.Label(self.addComputerTab, text="Computer Name*:")
        self.clabel2 = tkinter.Label(self.addComputerTab, text="IP Address:")
        self.clabel3 = tkinter.Label(self.addComputerTab, text="Status*:")
        self.clabel4 = tkinter.Label(self.addComputerTab, text="Model*:")
        self.clabel5 = tkinter.Label(self.addComputerTab, text="Serial Number*:")
        self.clabels = [self.clabel1, self.clabel2, self.clabel3, self.clabel4, self.clabel5]
        
        self.centry1 = tkinter.Entry(self.addComputerTab)
        self.centry2 = tkinter.Entry(self.addComputerTab)
        self.centry3 = tkinter.Entry(self.addComputerTab)
        self.centry4 = tkinter.Entry(self.addComputerTab)
        self.centry5 = tkinter.Entry(self.addComputerTab)
        self.centries = [self.centry1,self.centry2,self.centry3,self.centry4,self.centry5]

        self.submitButton = tkinter.Button(self.addComputerTab, text="Submit", command=lambda: self.submitNewComputer())

        for i in range(len(self.centries)):
            self.clabels[i].grid(row=i,column=0,padx=15,pady=10)
            self.centries[i].grid(row=i,column=1,padx=15,pady=10)

        self.submitButton.grid(row=len(self.centries),column=1,padx=15,pady=10)

        self.addComputerTab.pack()
        self.tab_parent.pack()

        self.mainloop()

    def submitNewComputer(self):
        result = Q.addNewComputer(self.centries[0].get(),self.centries[1].get(),self.centries[2].get(),
        self.centries[3].get(),self.centries[4].get()) #insert computer entry and check for success/failure
        match result:
            case 0: #successful add
                for entry in self.centries: #for each entry box
                    entry.delete(0, tkinter.END) #clear boxes
                    entry.insert(0, "")
                self.mainWin.forceUpdate() #force update to table
                self.mainWin.unsavedState() #mark unsaved state
                self.destroy() #close window
            case -1: NotificationWindow(-1) #necessary value is missing
            case -2: NotificationWindow(-2) #entry already exists
            case -3: NotificationWindow(-3) #something else went wrong idk what