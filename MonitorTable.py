import tksheet
import tkinter

import Queries as Q

monitorColumns = ["User", "Monitor 1", "Monitor 2"]

class MonitorSheet(tksheet.Sheet):

    def __init__(self, tab):
        super().__init__(tab)
        self.enable_bindings('single_select', 'rc_select', 'column_width_resize', 'copy')
        self.set_options(column_width=150)
        self.headers(monitorColumns)
        self.updateTableData()

    #force update all table data
    def updateTableData(self):
        tempData = Q.getData("Monitors")
        sheetData = [list(i) for i in tempData]
        self.set_sheet_data(sheetData, redraw=True)

    #filters for a given user
    def getOnlyUser(self, user):
        tempData = Q.getFilteredData("Monitors", "User", user)
        sheetData = [list(i) for i in tempData]
        self.set_sheet_data(sheetData, redraw=True)

    def assignMonitor1():
        pass

    def assignMonitor2():
        pass
