import tkinter
from tkinter.ttk import Notebook
from tkinter.ttk import Frame
from tkinter import StringVar

from ComputerTable import computerColumns

class FilterWindow(tkinter.Tk):
    def __init__(self,mainWindow):
        super().__init__()
        self.mainWin = mainWindow
        self.title('Filter')
        self.geometry('240x140')
        self['bg'] = '#BFBFBF'
        self.resizable(False,False)

        self.tab_parent = Notebook(self)
        self.filterTab = Frame(self.tab_parent)
        self.tab_parent.add(self.filterTab)

        self.filterLabel = tkinter.Label(self.filterTab, text="Filter by:")
        self.entryLabel = tkinter.Label(self.filterTab, text="Value:")

        self.stringStuff = StringVar(self.filterTab)
        self.stringStuff.set(computerColumns[0])
        self.options = tkinter.OptionMenu(self.filterTab, self.stringStuff, *computerColumns)
        self.options.config(width=15)

        self.enterValue = tkinter.Entry(self.filterTab)

        self.filterButton = tkinter.Button(self.filterTab, text="Apply Filter", command=lambda: self.applyFilter())

        self.filterLabel.grid(column=0,row=0,padx=15,pady=10)
        self.options.grid(column=1,row=0,padx=15,pady=10)
        self.entryLabel.grid(column=0,row=1,padx=15,pady=10)
        self.enterValue.grid(column=1,row=1,padx=15,pady=10)
        self.filterButton.grid(column=1,row=2,padx=15,pady=10)

        self.filterTab.pack()
        self.tab_parent.pack()

        self.mainloop()

    def applyFilter(self):
        column = self.stringStuff.get()
        match column:
            case "Computer Name": column = "ComputerName"
            case "IP Address": column = "IP"
            case "Serial Number": column = "SerialNumber"
            case "Assigned To": column = "AssignedTo"
        self.mainWin.filterUpdate(column,self.enterValue.get())