import tksheet

import Queries as Q

logColumns = ["Computer", "User", "Email", "Date"]

class LogSheet(tksheet.Sheet):

    def __init__(self, tab):
        super().__init__(tab)
        self.enable_bindings("column_width_resize", 'single_select')
        self.set_options(column_width=150)
        self.headers(logColumns)
        self.updateTableData()

    #forces update of the whole table
    def updateTableData(self):
        tempData = Q.getData("ComputerUsers")
        sheetData = [list(i) for i in tempData]
        self.set_sheet_data(sheetData, redraw=True)

    #updates the table based on the value of a selected column
    def filterTableData(self,column,value):
        pass