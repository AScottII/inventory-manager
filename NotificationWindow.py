import tkinter

class NotificationWindow(tkinter.Tk):

    def __init__(self, notif):
        super().__init__()

        self.title('Add New Computer')
        self.geometry('200x200')
        self['bg'] = '#BFBFBF'
        labelText = ""

        match notif:
            case -1: labelText = "One or more require fields are missing!"
            case -2: labelText = "This computer already exists!"
            case -3: labelText = "Database Error!"
            case -4: labelText = "Computer entered does not exist!"
            case _: labelText = "Error!"

        errorLabel = tkinter.Label(self,text=labelText)
        errorLabel.pack()

        errorButton = tkinter.Button(self,text="OK",command=lambda: self.close())
        errorButton.pack()

        self.mainloop()

    def close(self):
        self.destroy()