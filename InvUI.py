import tkinter
import sys

from ComputersTab import Tabs
import Queries as Q
from NewRecordWindow import NewRecordWindow
from FilterWindow import FilterWindow


dataBaseColumns = ["ComputerName", "IP", "Status", "Model", "SerialNumber", "AssignedTo", "Location"]

class InvUI(tkinter.Tk):

    def __init__(self):
        super().__init__()
        self.title('Inventory')
        self.geometry('800x600')
        self['bg'] = '#BFBFBF'
        self.resizable(False,False)

        self.tabs = Tabs(self)
        self.tabs.pack()

        #add buttons to tab1, honestly this should be done in the ComputersTab class
        self.FilterButton = tkinter.Button(self.tabs.tab1, text="Filter", command=lambda: self.filter(), width=30)
        self.FilterButton.pack()

        self.clearFilterButton = tkinter.Button(self.tabs.tab1, text="Clear Filter", command=lambda: self.forceUpdate(), width=30)
        self.clearFilterButton.pack()

        self.NewComputerButton = tkinter.Button(self.tabs.tab1,text="New Computer",command=lambda: self.newComputer(), width=30)
        self.NewComputerButton.pack()

        self.exportButton = tkinter.Button(self.tabs.tab1, text="Export", command=lambda: self.exportData(), width=30)
        self.exportButton.pack()

        self.addUserToMonitorButton = tkinter.Button(self.tabs.tab2, text="Add User", command=lambda: self.addUserToMonitors(), width=30)
        self.addUserToMonitorButton.pack()

        self.bind('<Control-s>', func=self.saveDatabase) #ctrl-s saves database
        self.bind('<Control-r>', func=self.revertChanges) #ctrl-r undoes changes to the database

        self.mainloop()

    #load entire unfiltered table to sheet
    def forceUpdate(self):
        self.tabs.forceUpdate()

    #load filtered data to sheet
    def filterUpdate(self,column, value):
        self.tabs.filterUpdate(column,value)

    def addUserToMonitors(self):
        pass

    #export currently loaded data to an excel spreadsheet
    def exportData(self):
        output = [dataBaseColumns]
        sheetData = self.tabs.CSheet.get_sheet_data()
        for lst in sheetData:
            output.append(lst)
        Q.exportData(output)

    #create a filter window
    def filter(self):
        FilterWindow(self)

    #create a new record window
    def newComputer(self):
        NewRecordWindow(self)

    #get computer of row of the cell selected
    def getComputerSelected(self):
        coord = list(self.tabs.CSheet.get_selected_cells())
        if (len(coord) != 1):
            return
        row = coord[0][0]

        return self.tabs.CSheet.get_cell_data(row,0,True)

    #commit changes to database
    def saveDatabase(self, event):
        Q.confirm()
        self.savedState()

    #revert changes and load from the last saved point in the database
    def revertChanges(self, event):
        Q.revert()
        self.forceUpdate()
        self.savedState()

    #set window title to show there are uncommitted changes
    def unsavedState(self):
        self.title('Inventory*')

    #set window to reflect that it matches the saved database
    def savedState(self):
        self.title('Inventory')

InvUI() #run the program