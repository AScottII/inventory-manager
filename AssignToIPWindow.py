import tkinter
from tkinter.ttk import Notebook, Frame

import Queries as Q

class AssignToIPWindow(tkinter.Tk):

    def __init__(self, mainWindow, computer):
        super().__init__()

        self.title('Assign IP Address')
        self.resizable(False, False)
        self.geometry('300x100')

        self.computerName = computer
        self.mainWin = mainWindow

        self.tab_parent = Notebook(self)
        self.assignIPTab = Frame(self.tab_parent)
        self.tab_parent.add(self.assignIPTab)

        self.label = tkinter.Label(self.assignIPTab, text="IP Address:")
        self.button = tkinter.Button(self.assignIPTab, text="Submit", command=lambda: self.submitIP())
        self.entry = tkinter.Entry(self.assignIPTab)

        self.label.grid(column=0,row=0,padx=15,pady=10)
        self.entry.grid(column=1,row=0,padx=15,pady=10)
        self.button.grid(column=1,row=1,padx=15,pady=10)

        self.assignIPTab.pack()
        self.tab_parent.pack()

    def submitIP(self):
        address = self.entry.get()
        Q.updateIPAddress(self.computerName, address)
        self.mainWin.updateTableData()
        self.mainWin.tab.master.master.unsavedState()
        self.destroy()


