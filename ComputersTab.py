from tkinter.ttk import Notebook
from tkinter.ttk import Frame
import tkinter

from ComputerTable import ComputerSheet
from LogTable import LogSheet
from MonitorTable import MonitorSheet

class Tabs(Notebook):
    
    def __init__(self, parentWindow):
        super().__init__(parentWindow)

        self.tab1 = Frame(self)
        self.tab2 = Frame(self)
        self.tab3 = Frame(self)

        self.CSheet = ComputerSheet(self.tab1)
        self.CSheet.pack(fill=tkinter.X)

        self.MSheet = MonitorSheet(self.tab2)
        self.MSheet.pack(fill=tkinter.X)

        self.LSheet = LogSheet(self.tab3)
        self.LSheet.pack(fill=tkinter.X)

        self.add(self.tab1, text="Computers")
        self.add(self.tab2, text="Monitors")
        self.add(self.tab3, text="Logs")

        self.pack(expand=1,fill='both')

    def forceUpdate(self):
        self.CSheet.updateTableData()
        self.LSheet.updateTableData()
        self.MSheet.updateTableData()

    def filterUpdate(self, column, value):
        self.CSheet.filterTableData(column, value)
