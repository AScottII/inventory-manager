import tksheet
import tkinter

from AssignToIPWindow import AssignToIPWindow
import Queries as Q
from SpecificationWindow import SpecificationWindow
from AssignToUI import AssignToUI

computerColumns = ["Computer Name", "IP Address", "Status", "Model", "Serial Number", "Assigned To", "Location"]

class ComputerSheet(tksheet.Sheet):

    def __init__(self, tab):
        super().__init__(tab)
        self.tab = tab
        self.headers(computerColumns)
        self.set_options(column_width=170)
        self.updateTableData()
        self.enable_bindings('single_select', 'rc_select', 'column_width_resize', 'copy')

        #right click context menu commands
        self.menu = tkinter.Menu(self, tearoff=0)
        self.menu.add_command(label="Get Specifications", command=lambda: self.getSpecs())
        self.menu.add_command(label="Assign to User", command=lambda: self.assignComputer())
        self.menu.add_command(label="Assign IP Address", command=lambda: self.assignIP())
        self.menu.add_command(label="Send to Storage", command=lambda: self.sendComputerToStorage())
        self.menu.add_separator()
        self.menu.add_command(label="Delete Computer", command=lambda: self.deleteComputer())

        self.bind("<Button-3>", self.do_popup) #right click opens context menu

    #assign a computer to an IP address
    def assignIP(self):
        computerName = self.getComputerSelected()
        if computerName != None:
            AssignToIPWindow(mainWindow=self,computer=computerName)

    #get the computer name from the row of the cell selected
    def getComputerSelected(self):
        coord = list(self.get_selected_cells())
        if (len(coord) != 1):
            return
        row = coord[0][0]

        return self.get_cell_data(row,0,True)

    #assign a computer to a given person in a given location
    def assignComputer(self):
        computerName = self.getComputerSelected()
        if computerName != None:
            AssignToUI(mainWindow=self,computer=computerName)

    #updates table data from database
    def updateTableData(self):
        tempData = Q.getSortedComputers()
        sheetData = [list(i) for i in tempData]
        self.set_sheet_data(sheetData, redraw=True)

    #updates table data with filtered data from data base
    def filterTableData(self,column, value):
        tempData = Q.getFilteredData("Computers", column, value, "ComputerName")
        sheetData = [list(i) for i in tempData]
        self.set_sheet_data(sheetData, redraw=True)

    #returns the specs of the device's model
    def getSpecs(self):
        coord = list(self.get_selected_cells())
        if (len(coord) != 1):
            return
        row = coord[0][0]
        model = self.get_cell_data(row,3,True)

        if Q.doesModelExist(model): SpecificationWindow(self.tab,model)

    #removes computer entry
    def deleteComputer(self):
        computerName = self.getComputerSelected()
        if computerName == None or not Q.doesEntryExist("Computers","ComputerName",computerName):
            return #don't delete if doesn't exist

        Q.deleteComputer(computerName)
        self.tab.master.forceUpdate()
        self.tab.master.master.unsavedState() #find a better way of doing this

    #sends computer to storage and then marks as ready to deploy
    def sendComputerToStorage(self):
        computerName = self.getComputerSelected()
        if computerName == None or not Q.doesEntryExist("Computers","ComputerName",computerName):
            return
        Q.sendComputerToStorage(computerName)
        Q.logComputerUser(computerName,"Storage")
        self.tab.master.forceUpdate()
        self.tab.master.master.unsavedState() #not an ideal way of doing this!

    #right-click menu trigger
    def do_popup(self, event):
        try: 
            self.menu.tk_popup(event.x_root,event.y_root)
        finally:
            self.menu.grab_release()

