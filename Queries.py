import sqlite3 as sql
from datetime import datetime
import pandas as pd

conn = sql.connect("Inventory.db")
cursor = conn.cursor()

#commits changes to DB
def confirm():
    conn.commit()

#rolls back any changes since last commit
def revert():
    conn.rollback()

#queries entire table
def getData(table):
    output = cursor.execute("SELECT * FROM %s" % table).fetchall()
    return output

#queries computers, sorts by computer name
def getSortedComputers():
    return cursor.execute('SELECT * FROM Computers ORDER BY ComputerName').fetchall()

#queries data from a table, filters by a value, sorts by a given column
def getFilteredData(table, column, value, sortColumn):
    if (not doesEntryExist(table,column,value)):
        return ("")
    value = '%' + value + '%'
    return cursor.execute("SELECT * FROM %s WHERE %s LIKE '%s' ORDER BY %s" % (table,column,value, sortColumn)).fetchall()

#queries all IP addresses that are assigned / are not empty
def getIPsInUse():
    output = cursor.execute("SELECT IP FROM Computers WHERE IP <> \'\'").fetchall()
    return output

#queries all computer names
def getAllComputerNames():
    return cursor.execute("SELECT ComputerName FROM Computers").fetchall()

#inserts a new entry into the computer table, catches errors as necessary
def addNewComputer(name,IP,model,status,serialNumber):
    if "" in {name,status,model,serialNumber}: return -1 #a NOT NULL variable is empty, return with condition -1 if this condition isn't met

    try:
        cursor.execute("INSERT INTO Computers(ComputerName,IP,Status,Model,SerialNumber,AssignedTo,Location) " +
        "VALUES('%s','%s','%s','%s','%s','','')" % (name,IP,model,status,serialNumber))
    except sql.IntegrityError:
        return -2 #constraint is violated, most likely due to a repeated UNIQUE key, return condition -2
    except sql.OperationalError:
        return -3 #something else goes wrong, return condition -3

    return 0 #successful operation, return condition 0

#removes computer from the table
def deleteComputer(computerName):
    cursor.execute("DELETE FROM Computers WHERE ComputerName='%s'" % computerName)

#queries entire row of a given computer
def getRowDataFromComputerName(ComputerName):
    return cursor.execute("SELECT * FROM Computers WHERE ComputerName='%s" % ComputerName)

#checks if a given cell exists within a table
def doesEntryExist(table,column,entry):
    output = cursor.execute("SELECT 1 FROM %s WHERE %s='%s'" % (table,column,entry)).fetchall()
    try:
        return output[0][0] == 1
    except:
        return False

#inserts a log entry of a computer transfer
def logComputerUser(computer, user):
    if user in ("Repair","Storage"):
        email = "roithelp@mcmaster.ca"
    else:
        email = user + "@mcmaster.ca"
    cursor.execute("INSERT INTO ComputerUsers(ComputerName,macID,Email,Date)" +
    "VALUES('%s','%s','%s','%s')" % (computer,user,email,datetime.now()))

#updates the status of a computer, should be in {Ready,Repair,Deployed}
def updateComputerStatus(computerName,status):
    cursor.execute("UPDATE Computers SET Status='%s' WHERE ComputerName='%s'" % (status,computerName))

#queries all current and past users of a given computer from the logs
def getAllUsersOfComputer(computer):
    return cursor.execute("SELECT DISTINCT macID FROM ComputerUsers " +
    "WHERE ComputerName='%s' AND macID <> 'repair' AND macID <> 'storage'" % computer).fetchall()

#updates computer status to move to storage
def sendComputerToStorage(computer):
    cursor.execute("UPDATE Computers SET Status='Ready', Location='Storage', AssignedTo='' " +
    "WHERE ComputerName ='%s'" % computer)

#updates computer state to repair
def sendComputerForRepair(computer):
    cursor.execute("UPDATE Computers SET Status='Repair', Location='Repair' " +
    "WHERE ComputerName = '%s'" % computer)

#assign a computer to a given user
def assignComputerToUser(computer,user,location):
    cursor.execute("UPDATE Computers SET Status='Deployed', AssignedTo='%s', Location='%s' " % (user,location) +
    "WHERE ComputerName='%s'" % computer)

#query the specs of a given computer/monitor model, if a corresponding entry exists
def getSpecs(model):
    computerType = []
    try:
        computerType = cursor.execute("SELECT Type FROM Specifications WHERE Model='%s'" % model).fetchall()[0][0]
    except:
        return
    specs = []
    match computerType:
        case "Desktop":
            specs = cursor.execute("SELECT HDD,RAM,VideoPorts,USBPorts,YearMade " +
            "FROM Specifications " +
            "WHERE Model='%s'" % model).fetchall()[0]
        case "Laptop": 
            specs = cursor.execute("SELECT Resolution,ScreenSize,HDD,RAM,VideoPorts,USBPorts,YearMade " +
            "FROM Specifications " +
            "WHERE Model='%s'" % model).fetchall()[0]
        case "Monitor": 
            specs = cursor.execute("SELECT Resolution,ScreenSize,VideoPorts,YearMade " +
            "FROM Specifications " + 
            "WHERE Model='%s'" % model).fetchall()[0]

    return computerType, specs

#queries if a model exists on record
def doesModelExist(model):
    output = cursor.execute("SELECT 1 FROM Specifications WHERE Model='%s'" % model).fetchall()
    try:
        return output[0][0] == 1
    except:
        return False

#updates computer's assigned IP address
def updateIPAddress(computerName, ip):
    cursor.execute("UPDATE Computers SET IP='%s' WHERE ComputerName='%s'" % (ip,computerName))

#exports currently displayed data on the computer tab to Excel
def exportData(data):
    df = pd.DataFrame(data)
    df.to_excel(excel_writer="output.xlsx", index=False)