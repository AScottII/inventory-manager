import tkinter
from tkinter.ttk import Notebook, Frame
from tkinter import StringVar

import Queries as Q

locations = ["A&A (GH-120)", "Admissions BSA (TSH-111)", "Admissions Officers (TSH-111)", "Recruitment (TSH-111)", "Scheduling & Exams (GH-114)",
"Records & Systems (GH-112)", "Student Services (GH-108)", "Comms (B102)"]
locations.sort()
locations.append("Other")

class AssignToUI(tkinter.Tk):

    def __init__(self,mainWindow,computer):
        super().__init__()
        self.mainWin = mainWindow
        self.computerName = computer
        self.title('Assign Computer to User')
        self.geometry('370x135')
        self['bg'] = '#BFBFBF'
        self.resizable(False, False)

        self.tab_parent = Notebook(self)
        self.assignComputerTab = Frame(self.tab_parent)
        self.tab_parent.add(self.assignComputerTab)

        self.label1 = tkinter.Label(self.assignComputerTab, text="User (macID):")
        self.label2 = tkinter.Label(self.assignComputerTab, text="Room Location:")

        self.submitButton = tkinter.Button(self.assignComputerTab, text="Submit", command=lambda: self.assignComputer())
        self.entryBox = tkinter.Entry(self.assignComputerTab)
        self.entryBox.config(width=35)
        
        self.locationList = StringVar(self.assignComputerTab)
        self.locationList.set(locations[0])
        self.dropDown = tkinter.OptionMenu(self.assignComputerTab, self.locationList, *locations)
        self.dropDown.config(width=30)

        self.label1.grid(row=0,column=0,padx=15,pady=10)
        self.label2.grid(row=1,column=0,padx=15,pady=10)
        self.entryBox.grid(row=0,column=1,padx=15,pady=10)
        self.dropDown.grid(row=1,column=1,padx=15,pady=10)
        self.submitButton.grid(row=2,column=1,padx=15,pady=10)

        self.tab_parent.pack()
        self.assignComputerTab.pack()

        self.mainloop()


    def assignComputer(self):
        location = self.locationList.get()
        user = self.entryBox.get()

        if (user == "" or user == None):
            return

        Q.assignComputerToUser(self.computerName,user,location)
        Q.logComputerUser(self.computerName,user)
        self.mainWin.tab.master.master.unsavedState() #top level window method call
        self.mainWin.tab.master.forceUpdate() #container with the tabs (since we need to update the logs as well)
        self.destroy()

